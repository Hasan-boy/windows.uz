const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        minlength: [2, 'Category name should be longer than 2 characters'],
        required: [true, 'Please add a name'],
        unique: [true, 'This category has already been created']
    },
    createdEt: {
        type: Date,
        default: Date.now()
    }
}/*,{
    toJSON: {virtuals : true},
    toObject: {virtuals: true}
}*/);

// Reverse populate with virtuals
// categorySchema.virtual('products',{
//     ref : 'Product',
//     localField: '_id',
//     foreignField: 'category',
//     justOne: false
// })

module.exports = mongoose.model('Category', categorySchema);