const mongoose = require('mongoose');
// const slugify = require('slugify');

const ProductSchema = new mongoose.Schema({
    name:{
        type: String,
        maxlength: 150,
        required: [true, 'Please add a name for the product']
    },
    category:{
        type: mongoose.Schema.ObjectId,
        ref: 'Category',
        requird: true
    },
    details: {
        type: String,
        required: [true, 'Please add a details'],
        maxlength: [500, 'Description can not be more than 500 characters']
      },
    info: {
        type: String,
        required: [true, 'Please add a description']
    },
    images:[
         {
            type: String,
            required: true
        }],
    price:{
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['old','new'],
        default: 'new'
    },
    slug: String,
    createdAt: {
        type: Date,
        default: Date.now
    }
});

// ProductSchema.pre('save', function(next){
//     this.slug = slugify(this.titleru, { lower: true });
//     next();
// });

module.exports = mongoose.model('Product',ProductSchema)