const path = require('path');
const asyncHandler = require('../middlewares/async');
const Product = require('../models/Product');
const Category = require('../models/Category');
const ErrorResponse = require('../utils/errorResponse');
// @desc      Get all product
// @route     GET /api/v1/product
// @access    Private/Admin
exports.getProduct = asyncHandler(async (req, res, next) => {

    let query;
    if (req.params.categoryId) {
        query = Product.findById(req.params.categoryId);
    } else {
        query = Product.find();
    }
    const products = await query;
    res.status(200).json({
        success: true,
        data: products
    });
});



// @desc      Create product
// @route     POST /api/v1/product
// @access    Private/Admin
exports.createProduct = asyncHandler(async (req, res, next) => {
    req.body.category = req.params.categoryId;
    const category = await Category.findById(req.params.categoryId);
    if (!category) {
        return next(new ErrorResponse(`No category with id of ${req.params.categoryId}`, 404));
    }
    // const {name, details, info, price} = req.body;
    if (!req.files) {
        return next(new ErrorResponse(`Please ulpoad a file`, 400));
    }

    const file = req.files.file;

    // Make sure the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please ulpoad an image file`, 400));
    }

    // Check file size
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(new ErrorResponse(`Please upload an image less than 
        ${process.env.MAX_FILE_UPLOAD}`, 400));
    }
    let product = await Product.create(req.body);
    // Create custom file name
    file.name = `photo_${product._id}_${product.images.length}${path.parse(file.name).ext}`;
    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
            console.error(err);
            return next(new ErrorResponse(`Problem with file upload`, 500));
        }
    });
    product = await Product.findByIdAndUpdate(product._id,{
        images : file.name
    },{
        new: true,
        runValidators: true
    });
    res.status(201).json({
        success: true,
        data: product
    })
});

// @desc      Update product
// @route     PUT /api/product/:id
// @access    Private/Admin
exports.updateProduct = asyncHandler(async (req, res, next) => {
    const product = await Product.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: product
    });
});

// @desc      Delete product
// @route     DELETE /api/product/:id
// @access    Private/Admin
exports.deleteProduct = asyncHandler(async (req, res, next) => {
    await Product.findByIdAndDelete(req.params.id);

    res.status(200).json({
        success: true,
        data: {}
    });
});

// @desc      Get Product By Id
// @route     GET /api/product/all/:id
// @access    PUBLIC
exports.getProductById = asyncHandler(async (req, res, next) => {
    const product = await Product.find({ _id: req.params.id });

    res.status(200).json({
        success: true,
        data: product
    });
});


// @desc      Get all product
// @route     GET /api/v1/product
// @access    Private/Admin
/*exports.getProductByUser = asyncHandler(async (req, res, next) => {
    const query = { state: 'OK' };
    const n = Product.count(query);
    const r = Math.floor(Math.random() * n);
    const topThree = await Product.find().skip(r).limit(3);
    const popular = await Product.find().sort({ rating: -1 }).limit(8);
    const newProduct = await Product.find().sort({ createdAt: -1 }).limit(8);
    const budget = await Product.find().sort({ price: 1 }).limit(8);
    const best = await Product.find().sort({ price: -1 }).limit(8)
    res.status(200).json({
        success: true,
        data: {
            topThree,
            popular,
            newProduct,
            budget,
            best
        }
    })
})*/
