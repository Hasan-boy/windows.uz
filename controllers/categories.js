const Category = require('../models/Category');
const asyncHandler = require('../middlewares/async');
const { query } = require('express');

//@desc Get all Categories
//@route GET api/category
//@access Public
exports.getAllCateogry = asyncHandler(async (req, res, next) =>{
    const cateogories = await Category.find();
    res.status(200).json({
        success : true,
        data : cateogories
    });
});

//@desc Get single Category
//@route GET api/category
//@access Public
exports.getCateogry = asyncHandler(async (req, res, next) =>{
    const cateogory = await Category.findById(req.params.id);
    res.status(200).json({
        success : true,
        data : cateogory
    });
});

//@desc Create Category
//@route Post api/category
//@access Private
exports.createCateogry = asyncHandler(async (req, res, next) =>{
    const cateogory = await Category.create(req.body);
    res.status(200).json({
        success : true,
        data : cateogory
    });
});

//@desc Update Category
//@route PUT api/category/:id
//@access Private
exports.updateCateogry = asyncHandler(async (req, res, next) =>{
    const cateogory = await Category.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });
    res.status(200).json({
        success : true,
        data : cateogory
    });
});

//@desc Delete Category
//@route Delete api/category:id
//@access Private
exports.deleteCateogry = asyncHandler(async (req, res, next) =>{
    const cateogory = await Category.findByIdAndDelete(req.params.id);
    res.status(200).json({
        success : true,
        data : cateogory
    });
});

// Finding resource 
// query = Category.find(JSON.parse(queryStr)).populate(products);