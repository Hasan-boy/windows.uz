const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const colors = require('colors')
const fileupload = require('express-fileupload');
const connectDB = require('./config/db');
const errorHandler = require('./middlewares/error');
const bodyparser = require('body-parser')

const app = express();

// Load env vars
dotenv.config({ path: './config/config.env' });

// Connect to database
connectDB();

// File uploading
app.use(fileupload());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Route files
const users = require('./routes/user');
const auth = require('./routes/auth');
const category = require('./routes/category');
const product = require('./routes/product');
// require('./routes/prod')(app);

// Body parser
app.use(bodyparser.json());
app.use(express.urlencoded());

// Mount routers
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/categories', category);
app.use('/api/products', product);

app.use(errorHandler);
app.get('/', (req,res)=>{
  res.send("Hello Server")
})
const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);
