const express = require('express');
const {
    getAllCateogry,
    getCateogry,
    createCateogry,
    updateCateogry,
    deleteCateogry
} = require('../controllers/categories');

// Include other routers
const productRouter = require('./product');

const router = express.Router();
const {protect, authorize} = require('../middlewares/auth');

// Re-route into other resource routers
router.use('/:categoryId/products',productRouter);

router
.use(protect,authorize('admin'))
.route('/')
.post(createCateogry)
router
.route('/')
.get(getAllCateogry)
router
.route('/:id')
.get(getCateogry)
.put(protect,authorize('admin'),updateCateogry)
.delete(protect,authorize('admin'),deleteCateogry)

module.exports = router;