const express = require('express')
// const multer = require('multer')
const {
    getProduct,
    createProduct,
    updateProduct,
    deleteProduct,
    getProductById,
    // getProductByUser
} = require('../controllers/products')
const router = express.Router({ mergeParams: true });
const { protect, authorize } = require('../middlewares/auth');


router
    .route('/')
    .get(getProduct)
    .post(protect, authorize('admin'), createProduct);

// router.get('/all', getProduct)
// router.get('/sorted',getProductByUser)
// router.get('/all/:id', getProductById)

router
    .route('/:id')
    .put(protect, authorize('admin', 'publisher'), updateProduct)
    .delete(protect, authorize('admin', 'publisher'), deleteProduct);

module.exports = router;
